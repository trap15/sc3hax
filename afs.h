/*
	sc3hax -- Various tools for Soul Calibur 3
	AFS reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef AFS_H_
#define AFS_H_

#include "tools.h"

typedef struct {
	uint32_t offset;
	void* data;
	uint32_t size;
	char fname[32];
	uint16_t year, month, day, hour, minute, second;
} Afs_entry;

typedef struct {
	uint32_t count; /* File count */
	Afs_entry* entries;
} Afs;

Afs* load_afs(void* data, uint32_t size);
void* compile_afs(Afs* afs, uint32_t* size);
void delete_afs(Afs* afs);

#endif

