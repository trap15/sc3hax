/*
	sc3hax -- Various tools for Soul Calibur 3
	Soul Calibur 3 Model interpreter

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <string.h>

#include "memfile.h"
#include "tools.h"
#include "sc3mdl.h"

Sc3mdl* load_sc3mdl(void* data, uint32_t size)
{
	Mem* mp;
	Sc3mdl* mdl;
	int i;
	mp = mopen(data, size, 1);
	if(mp == NULL) {
		fprintf(stderr, "Unable to open memory\n");
		return NULL;
	}
	mdl = malloc(sizeof(Sc3mdl));
	mclose(mp);
	return mdl;
}

void* compile_sc3mdl(Sc3mdl* mdl, uint32_t* size)
{
	return NULL;
}

void delete_sc3mdl(Sc3mdl* mdl)
{
	free(mdl);
}


