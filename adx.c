/*
	sc3hax -- Various tools for Soul Calibur 3
	ADX->WAV converter

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <string.h>

#include "memfile.h"
#include "tools.h"
#include "adx.h"

#define BASEVOL 0x4000
#define S1SCALE 0x7298
#define S2SCALE 0x3350

#define MAXCHANS 4

static int32_t interp_samples[MAXCHANS][2];

static int32_t convertadxsample(int8_t samp, int32_t scale, int chan)
{
	int32_t ret;
	if(samp & 8)
		samp -= 0x10;
	ret = ((BASEVOL * samp * scale) +
		(S1SCALE * interp_samples[chan][0]) -
		(S2SCALE * interp_samples[chan][1])) >> 14;
	/* Clamp */
	if(ret >  32767) ret =  32767;
	if(ret < -32768) ret = -32768;
	interp_samples[chan][1] = interp_samples[chan][0];
	interp_samples[chan][0] = ret;
	return ret;
}

static void convertadx(uint8_t *inbuf, uint16_t *outbuf, int chan)
{
	int i;
	int32_t scale = (inbuf[0] << 8) | inbuf[1];
	inbuf += 2;
	for(i = 0; i < 16; i++) {
		*outbuf++ = convertadxsample(inbuf[i] >> 4, scale, chan);
		*outbuf++ = convertadxsample(inbuf[i] & 0xF, scale, chan);
	}
}

Adx* load_adx(void* data, uint32_t size)
{
	Mem* mp;
	Adx* adx;
	int i;
	mp = mopen(data, size, 1);
	if(mp == NULL) {
		fprintf(stderr, "Unable to open memory\n");
		return NULL;
	}
	adx = malloc(sizeof(Adx));
	uint8_t head[0x30];
	mread(head, 0x30, 1, mp);
	uint32_t off = be32(*(uint32_t*)(head)) + 4 - 0x80000000;
	adx->channels = head[7];
	adx->freq = be32(*(uint32_t*)(head + 0x08));
	adx->samples = be32(*(uint32_t*)(head + 0x0C));
	adx->loop_begin = be32(*(uint32_t*)(head + 0x1C));
	adx->loop_top = be32(*(uint32_t*)(head + 0x20));
	adx->loop_length = be32(*(uint32_t*)(head + 0x24));
	adx->loop_end = be32(*(uint32_t*)(head + 0x28));
	mseek(mp, off, SEEK_SET);
	uint32_t samps = adx->samples;
	Mem* d_mp = mopen(NULL, adx->samples * adx->channels * 2, 2);
	for(i = 0; i < adx->channels; i++) {
		interp_samples[i][0] = interp_samples[i][1] = 0;
	}
	while(samps) {
		uint8_t inbuf[18];
		uint16_t outbufs[MAXCHANS][32];
		uint32_t write_samps;
		for(i = 0; i < adx->channels; i++) {
			mread(inbuf, 18, 1, mp);
			convertadx(inbuf, outbufs[i], i);
		}
		if(samps > 32)
			write_samps = 32;
		else
			write_samps = samps;
		samps -= write_samps;
		int l;
		for(l = 0; l < write_samps; l++) {
			for(i = 0; i < adx->channels; i++) {
				mwrite(&outbufs[i][l], 2, 1, d_mp);
			}
		}
	}
	adx->data = mgetm(d_mp);
	mclose(d_mp);
	mclose(mp);
	return adx;
}

void* compile_adx(Adx* adx, uint32_t* size)
{
	return NULL;
}

void delete_adx(Adx* adx)
{
	free(adx->data);
	free(adx);
}


