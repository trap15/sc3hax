TARGETS   = afs.a adx.a olnk.a pkg.a sc3mdl.a memfile.a sc3hax
OBJECTS   = sc3hax.o memfile.o afs.o adx.o olnk.o pkg.o sc3mdl.o
LIBS      = 
CFLAGS    = -O3 $(INCLUDE)
LDFLAGS   = $(LIBS) -g
CLEANED   = $(OBJECTS) $(TARGETS)

.PHONY: all clean

all: $(OBJECTS) $(TARGETS)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

memfile.a: memfile.h memfile.o
	$(AR) rcs $@ memfile.o

afs.a: memfile.h memfile.a afs.h afs.o
	$(AR) rcs $@ afs.o

adx.a: memfile.h memfile.a adx.h adx.o
	$(AR) rcs $@ adx.o

olnk.a: memfile.h memfile.a olnk.h olnk.o
	$(AR) rcs $@ olnk.o

pkg.a: memfile.h memfile.a pkg.h pkg.o
	$(AR) rcs $@ pkg.o

sc3mdl.a: memfile.h memfile.a sc3mdl.h sc3mdl.o
	$(AR) rcs $@ sc3mdl.o

sc3hax: adx.a adx.h afs.a afs.h olnk.a olnk.h pkg.a pkg.h sc3mdl.a sc3mdl.h sc3hax.o
	$(CC) sc3hax.o adx.a afs.a olnk.a pkg.a sc3mdl.a memfile.a $(LDFLAGS) -o $@

clean:
	$(RM) $(CLEANED)

