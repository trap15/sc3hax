/*
	sc3hax -- Various tools for Soul Calibur 3
	AFS reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <string.h>

#include "memfile.h"
#include "tools.h"
#include "afs.h"

Afs* load_afs(void* data, uint32_t size)
{
	Mem* mp;
	Afs* afs;
	int i;
	char head[4];
	uint32_t fd_off, fd_len;
	mp = mopen(data, size, 1);
	if(mp == NULL) {
		fprintf(stderr, "Unable to open memory\n");
		return NULL;
	}
	mread(head, 4, 1, mp);
	if(memcmp(head, "AFS\0", 4) != 0) {
		fprintf(stderr, "Bad magic\n");
		mclose(mp);
		return NULL;
	}
	afs = malloc(sizeof(Afs));
	mread(&afs->count, sizeof(uint32_t), 1, mp);
	afs->count = le32(afs->count) & 0xFFFF;
	afs->entries = calloc(sizeof(Afs_entry), afs->count);
	for(i = 0; i < afs->count; i++) {
		mread(&afs->entries[i].offset, sizeof(uint32_t), 1, mp);
		afs->entries[i].offset = (le32(afs->entries[i].offset) + 2047) & ~2047;
		mread(&afs->entries[i].size, sizeof(uint32_t), 1, mp);
		afs->entries[i].size = (le32(afs->entries[i].size) + 2047) & ~2047;
		if(afs->entries[i].size != 0)
			afs->entries[i].data = malloc(afs->entries[i].size);
	}
	mread(&fd_off, sizeof(uint32_t), 1, mp);
	fd_off = (le32(fd_off) + 2047) & ~2047;
	mread(&fd_len, sizeof(uint32_t), 1, mp);
	fd_len = (le32(fd_len) + 2047) & ~2047;
	for(i = 0; i < afs->count; i++) {
		if(afs->entries[i].size != 0) {
			mseek(mp, afs->entries[i].offset, SEEK_SET);
			mread(afs->entries[i].data, afs->entries[i].size, 1, mp);
			if(fd_len != 0) {
				mseek(mp, fd_off + (i * 0x30), SEEK_SET);
				mread(afs->entries[i].fname, 0x20, 1, mp);
				mread(&afs->entries[i].year, 2, 1, mp);
				mread(&afs->entries[i].month, 2, 1, mp);
				mread(&afs->entries[i].day, 2, 1, mp);
				mread(&afs->entries[i].hour, 2, 1, mp);
				mread(&afs->entries[i].minute, 2, 1, mp);
				mread(&afs->entries[i].second, 2, 1, mp);
				afs->entries[i].year = le16(afs->entries[i].year);
				afs->entries[i].month = le16(afs->entries[i].month);
				afs->entries[i].day = le16(afs->entries[i].day);
				afs->entries[i].hour = le16(afs->entries[i].hour);
				afs->entries[i].minute = le16(afs->entries[i].minute);
				afs->entries[i].second = le16(afs->entries[i].second);
			}
		}
	}
	mclose(mp);
	return afs;
}

void* compile_afs(Afs* afs, uint32_t* size)
{
	return NULL;
}

void delete_afs(Afs* afs)
{
	int i;
	for(i = 0; i < afs->count; i++) {
		free(afs->entries[i].data);
	}
	free(afs->entries);
	free(afs);
}

