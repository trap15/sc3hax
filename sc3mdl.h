/*
	sc3hax -- Various tools for Soul Calibur 3
	Soul Calibur 3 Model interpreter

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef SC3MDL_H_
#define SC3MDL_H_

#include "tools.h"

typedef struct {
	float vert[3];
	float weight;
	float norm[3];
	uint8_t boneidx; /* ??? */
	uint8_t mode;
	float tex[2];
	float connect;
	uint32_t ends[4];
} Sc3vtx;

typedef enum {
	SC3CHUNK_UNK = 0,
	SC3CHUNK_VTX,
} Sc3chunktype;

typedef struct {
	Sc3chunktype type;
	union {
		struct {
			uint32_t vtxcnt;
			Sc3vtx* vtxs;
		};
	};
} Sc3chunk;

typedef struct {
	uint32_t chunkcnt;
	Sc3chunk* chunks;
} Sc3mdl;

Sc3mdl* load_sc3mdl(void* data, uint32_t size);
void* compile_sc3mdl(Sc3mdl* mdl, uint32_t* size);
void delete_sc3mdl(Sc3mdl* mdl);

#endif

