/*
	sc3hax -- Various tools for Soul Calibur 3
	OLNK reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <string.h>

#include "memfile.h"
#include "tools.h"
#include "olnk.h"

Olnk* load_olnk(void* data, uint32_t size)
{
	Mem* mp;
	Olnk* olnk;
	mp = mopen(data, size, 1);
	if(mp == NULL) {
		fprintf(stderr, "Unable to open memory\n");
		return NULL;
	}
	uint32_t fcount;
	mread(&fcount, 4, 1, mp);
	fcount = le32(fcount);
	char head[4];
	mread(head, 4, 1, mp);
	if(memcmp(head, "olnk", 4) != 0) {
		fprintf(stderr, "Bad magic\n");
		mclose(mp);
		return NULL;
	}
	olnk = malloc(sizeof(Olnk));
	olnk->count = fcount;
	mread(&olnk->data_off, 4, 1, mp);
	olnk->data_off = le32(olnk->data_off);
	olnk->entries = calloc(olnk->count + 1, sizeof(Olnk_entry));
	mseek(mp, 4, SEEK_CUR);
	mread(&olnk->me.offset, 4, 1, mp);
	olnk->me.offset = le32(olnk->me.offset);
	mread(&olnk->me.size, 4, 1, mp);
	olnk->me.size = le32(olnk->me.size);
	mread(&olnk->me.unk1, 4, 1, mp);
	olnk->me.unk1 = le32(olnk->me.unk1);
	mread(&olnk->me.unk2, 4, 1, mp);
	olnk->me.unk2 = le32(olnk->me.unk2);
	uint32_t i;
	for(i = 0; i < olnk->count; i++) {
		Olnk_entry* ent = olnk->entries + i;
		mread(&ent->offset, 4, 1, mp);
		ent->offset = le32(ent->offset);
		mread(&ent->size, 4, 1, mp);
		ent->size = le32(ent->size);
		mread(&ent->unk1, 4, 1, mp);
		ent->unk1 = le32(ent->unk1);
		mread(&ent->unk2, 4, 1, mp);
		ent->unk2 = le32(ent->unk2);
	}
	for(i = 0; i < olnk->count; i++) {
		Olnk_entry* ent = olnk->entries + i;
		mseek(mp, ent->offset + olnk->data_off, SEEK_SET);
		ent->data = malloc(ent->size);
		mread(ent->data, ent->size, 1, mp);
	}
	mclose(mp);
	return olnk;
}

void* compile_olnk(Olnk* olnk, uint32_t* size)
{
	return NULL;
}

void delete_olnk(Olnk* olnk)
{
	int i;
	for(i = 0; i < olnk->count; i++) {
		free(olnk->entries[i].data);
	}
	free(olnk->entries);
	free(olnk);
}

