/*
	sc3hax -- Various tools for Soul Calibur 3
	PKG reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef PKG_H_
#define PKG_H_

#include "tools.h"

typedef struct {
	uint32_t offset;
	uint32_t size;
	void* data;
} Pkg_entry;

typedef struct {
	uint32_t count; /* File count */
	Pkg_entry* entries;
} Pkg;

Pkg* load_pkg(void* data, uint32_t size);
void* compile_pkg(Pkg* pkg, uint32_t* size);
void delete_pkg(Pkg* pkg);

#endif

