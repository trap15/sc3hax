/*
	sc3hax -- Various tools for Soul Calibur 3
	Misc services

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef TOOLS_H_
#define TOOLS_H_

#include <arpa/inet.h>

static inline uint32_t es32(uint32_t val)
{
	return	((val & 0x000000FF) << 24) |
		((val & 0x0000FF00) <<  8) |
		((val & 0x00FF0000) >>  8) |
		((val & 0xFF000000) >> 24);
}

static inline uint16_t es16(uint16_t val)
{
	return	((val & 0x00FF) << 8) |
		((val & 0xFF00) >> 8);
}

static inline uint32_t le32(uint32_t val)
{
	return ntohl(es32(val));
}

static inline uint16_t le16(uint16_t val)
{
	return ntohs(es16(val));
}

static inline uint32_t be32(uint32_t val)
{
	return ntohl(val);
}

static inline uint16_t be16(uint16_t val)
{
	return ntohs(val);
}

#endif

