/*
	sc3hax -- Various tools for Soul Calibur 3
	ADX reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef ADX_H_
#define ADX_H_

#include "tools.h"

typedef struct {
	int channels;
	uint32_t freq;
	uint32_t samples;
	uint32_t loop_begin;
	uint32_t loop_top;
	uint32_t loop_length;
	uint32_t loop_end;
	void* data;
} Adx;

Adx* load_adx(void* data, uint32_t size);
void* compile_adx(Adx* adx, uint32_t* size);
void delete_adx(Adx* adx);

#endif

