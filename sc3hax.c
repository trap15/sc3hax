/*
	sc3hax -- Various tools for Soul Calibur 3
	All-encompassing tool

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "afs.h"
#include "adx.h"
#include "olnk.h"
#include "pkg.h"
#include "sc3mdl.h"

void usage(char *app)
{
	fprintf(stderr,
		"Invalid arguments. Usage:\n"
		"	%s <afs|adx|olnk|pkg|sc3mdl> [params]\n",
		app);
}

void usage_afs(char *app)
{
	fprintf(stderr,
		"Invalid arguments. Usage:\n"
		"	%s afs input.afs [outfolder]\n",
		app);
}

void usage_adx(char *app)
{
	fprintf(stderr,
		"Invalid arguments. Usage:\n"
		"	%s adx input.adx [out.wav]\n",
		app);
}

void usage_olnk(char *app)
{
	fprintf(stderr,
		"Invalid arguments. Usage:\n"
		"	%s olnk input.olk [outfolder]\n",
		app);
}

void usage_pkg(char *app)
{
	fprintf(stderr,
		"Invalid arguments. Usage:\n"
		"	%s pkg input.pkg [outfolder]\n",
		app);
}

void usage_sc3mdl(char *app)
{
	fprintf(stderr,
		"Invalid arguments. Usage:\n"
		"	%s sc3mdl model.mdl\n",
		app);
}

int handle_afs(int argc, char *argv[])
{
	size_t sz;
	void* buf;
	Afs* afs;
	if(argc < 3) {
		usage_afs(argv[0]);
		return EXIT_FAILURE;
	}
	FILE* fp = fopen(argv[2], "rb");
	if(fp == NULL) {
		fprintf(stderr, "Unable to open %s\n", argv[2]);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	buf = malloc(sz);
	if(buf == NULL) {
		fprintf(stderr, "Unable to allocate\n");
		fclose(fp);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_SET);
	fread(buf, sz, 1, fp);
	fclose(fp);
	afs = load_afs(buf, sz);
	int i;
	char* dirfn;
	if(argc >= 4) {
		asprintf(&dirfn, "%s/", argv[3]);
	}else{
		asprintf(&dirfn, "afsout/");
	}
	mkdir(dirfn, S_IRWXU | S_IRWXG | S_IRWXO);
	for(i = 0; i < afs->count; i++) {
		Afs_entry* ent = &afs->entries[i];
		if(ent->size == 0)
			continue;
		printf("File %d: %s (0x%08X bytes) [%04d/%02d/%02d %02d:%02d:%02d]\n",
			i, ent->fname, ent->size, ent->year, ent->month, ent->day,
			ent->hour, ent->minute, ent->second);
		char* outfn;
		char* dotpch = strrchr(ent->fname, '.');
		if(dotpch) {
			*dotpch = 0;
			if(ent->fname[0] == 0)
				asprintf(&outfn, "%s%08X.%s", dirfn, i, dotpch+1);
			else
				asprintf(&outfn, "%s%s_%08X.%s", dirfn, ent->fname, i, dotpch+1);
		}else{
			if(ent->fname[0] == 0)
				asprintf(&outfn, "%s%08X", dirfn, i);
			else
				asprintf(&outfn, "%s%s_%08X", dirfn, ent->fname, i);
		}
		char *ndirfn, *ndirfn2;
		asprintf(&ndirfn, "%s", dirfn);
		char* pch = strtok(ent->fname, "/");
		char* npch = strtok(NULL, "/");
		while(npch != NULL) {
			asprintf(&ndirfn2, "%s%s/", ndirfn, pch);
			free(ndirfn);
			asprintf(&ndirfn, "%s", ndirfn2);
			free(ndirfn2);
			mkdir(ndirfn, S_IRWXU | S_IRWXG | S_IRWXO);
			pch = npch;
			npch = strtok(NULL, "/");
		}
		free(ndirfn);
		fp = fopen(outfn, "wb");
		if(fp == NULL) {
			fprintf(stderr, "Unable to open %s\n", outfn);
			delete_afs(afs);
			return EXIT_FAILURE;
		}
		fwrite(ent->data, ent->size, 1, fp);
		fclose(fp);
		free(outfn);
	}
	free(dirfn);
	delete_afs(afs);
	return EXIT_SUCCESS;
}

int handle_adx(int argc, char *argv[])
{
	size_t sz;
	void* buf;
	Adx* adx;
	if(argc < 3) {
		usage_adx(argv[0]);
		return EXIT_FAILURE;
	}
	FILE* fp = fopen(argv[2], "rb");
	if(fp == NULL) {
		fprintf(stderr, "Unable to open %s\n", argv[2]);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	buf = malloc(sz);
	if(buf == NULL) {
		fprintf(stderr, "Unable to allocate\n");
		fclose(fp);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_SET);
	fread(buf, sz, 1, fp);
	fclose(fp);
	adx = load_adx(buf, sz);
	char* fname;
	if(argc < 4) {
		asprintf(&fname, "%s.wav", argv[2]);
	}else{
		asprintf(&fname, "%s", argv[3]);
	}
	printf("Freq %d\nSamples %d\nChannels %d\n", adx->freq, adx->samples, adx->channels);
	/* Horrible WAVE format code. Please don't read this */
	fp = fopen(fname, "wb");
	fwrite("RIFF", 4, 1, fp);
	uint32_t size = es32(htonl(adx->samples * adx->channels * 2 + 8 + 0x1C));
	fwrite(&size, 4, 1, fp);
	fwrite("WAVEfmt ", 8, 1, fp);
	uint32_t tmp32 = es32(htonl(0x10));
	fwrite(&tmp32, 4, 1, fp);
	uint16_t tmp16 = es16(htons(1));
	fwrite(&tmp16, 2, 1, fp);
	tmp16 = es16(htons(adx->channels));
	fwrite(&tmp16, 2, 1, fp);
	tmp32 = es32(htonl(adx->freq));
	fwrite(&tmp32, 4, 1, fp);
	tmp32 = es32(htonl(adx->freq * adx->channels * 2));
	fwrite(&tmp32, 4, 1, fp);
	tmp16 = es16(htons(adx->channels * 2));
	fwrite(&tmp16, 2, 1, fp);
	tmp16 = es16(htons(16));
	fwrite(&tmp16, 2, 1, fp);
	fwrite("data", 4, 1, fp);
	size = es32(htonl(adx->samples * adx->channels * 2));
	fwrite(&size, 4, 1, fp);
	fwrite(adx->data, adx->freq * adx->samples * adx->channels, 2, fp);
	fclose(fp);
	free(fname);
	delete_adx(adx);
	return EXIT_SUCCESS;
}

int handle_olnk(int argc, char *argv[])
{
	size_t sz;
	void* buf;
	Olnk* olnk;
	if(argc < 3) {
		usage_olnk(argv[0]);
		return EXIT_FAILURE;
	}
	FILE* fp = fopen(argv[2], "rb");
	if(fp == NULL) {
		fprintf(stderr, "Unable to open %s\n", argv[2]);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	buf = malloc(sz);
	if(buf == NULL) {
		fprintf(stderr, "Unable to allocate\n");
		fclose(fp);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_SET);
	fread(buf, sz, 1, fp);
	fclose(fp);
	olnk = load_olnk(buf, sz);
	int i;
	char* dirfn;
	if(argc >= 4) {
		asprintf(&dirfn, "%s/", argv[3]);
	}else{
		asprintf(&dirfn, "olnkout/");
	}
	mkdir(dirfn, S_IRWXU | S_IRWXG | S_IRWXO);
	for(i = 0; i < olnk->count; i++) {
		Olnk_entry* ent = &olnk->entries[i];
		if(ent->size == 0)
			continue;
		printf("File %d: (0x%08X bytes) [0x%08X 0x%08X]\n",
			i, ent->size, ent->unk1, ent->unk2);
		char* outfn;
		asprintf(&outfn, "%sFile%08X.bin", dirfn, i);
		fp = fopen(outfn, "wb");
		if(fp == NULL) {
			fprintf(stderr, "Unable to open %s\n", outfn);
			delete_olnk(olnk);
			return EXIT_FAILURE;
		}
		fwrite(ent->data, ent->size, 1, fp);
		fclose(fp);
		free(outfn);
	}
	free(dirfn);
	delete_olnk(olnk);
	return EXIT_SUCCESS;
}

int handle_pkg(int argc, char *argv[])
{
	size_t sz;
	void* buf;
	Pkg* pkg;
	if(argc < 3) {
		usage_pkg(argv[0]);
		return EXIT_FAILURE;
	}
	FILE* fp = fopen(argv[2], "rb");
	if(fp == NULL) {
		fprintf(stderr, "Unable to open %s\n", argv[2]);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	buf = malloc(sz);
	if(buf == NULL) {
		fprintf(stderr, "Unable to allocate\n");
		fclose(fp);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_SET);
	fread(buf, sz, 1, fp);
	fclose(fp);
	pkg = load_pkg(buf, sz);
	int i;
	char* dirfn;
	if(argc >= 4) {
		asprintf(&dirfn, "%s/", argv[3]);
	}else{
		asprintf(&dirfn, "pkgout/");
	}
	mkdir(dirfn, S_IRWXU | S_IRWXG | S_IRWXO);
	for(i = 0; i < pkg->count; i++) {
		Pkg_entry* ent = &pkg->entries[i];
		if(ent->size == 0)
			continue;
		printf("File %d: (0x%08X bytes)\n",
			i, ent->size);
		char* outfn;
		asprintf(&outfn, "%sFile%08X.bin", dirfn, i);
		fp = fopen(outfn, "wb");
		if(fp == NULL) {
			fprintf(stderr, "Unable to open %s\n", outfn);
			delete_pkg(pkg);
			return EXIT_FAILURE;
		}
		fwrite(ent->data, ent->size, 1, fp);
		fclose(fp);
		free(outfn);
	}
	free(dirfn);
	delete_pkg(pkg);
	return EXIT_SUCCESS;
}

int handle_sc3mdl(int argc, char *argv[])
{
	size_t sz;
	void* buf;
	Sc3mdl* mdl;
	if(argc < 3) {
		usage_sc3mdl(argv[0]);
		return EXIT_FAILURE;
	}
	FILE* fp = fopen(argv[2], "rb");
	if(fp == NULL) {
		fprintf(stderr, "Unable to open %s\n", argv[2]);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	buf = malloc(sz);
	if(buf == NULL) {
		fprintf(stderr, "Unable to allocate\n");
		fclose(fp);
		return EXIT_FAILURE;
	}
	fseek(fp, 0, SEEK_SET);
	fread(buf, sz, 1, fp);
	fclose(fp);
	mdl = load_sc3mdl(buf, sz);
	delete_sc3mdl(mdl);
	return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{
	if(argc < 2) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	if(strcmp(argv[1], "afs") == 0) {
		return handle_afs(argc, argv);
	}else if(strcmp(argv[1], "adx") == 0) {
		return handle_adx(argc, argv);
	}else if(strcmp(argv[1], "olnk") == 0) {
		return handle_olnk(argc, argv);
	}else if(strcmp(argv[1], "pkg") == 0) {
		return handle_pkg(argc, argv);
	}else if(strcmp(argv[1], "sc3mdl") == 0) {
		return handle_sc3mdl(argc, argv);
	}
	usage(argv[0]);
	return EXIT_FAILURE;
}

