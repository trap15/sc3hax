/*
	sc3hax -- Various tools for Soul Calibur 3
	PKG reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <string.h>

#include "memfile.h"
#include "tools.h"
#include "pkg.h"

Pkg* load_pkg(void* data, uint32_t size)
{
	Mem* mp;
	Pkg* pkg;
	mp = mopen(data, size, 1);
	if(mp == NULL) {
		fprintf(stderr, "Unable to open memory\n");
		return NULL;
	}
	uint32_t fcount;
	mread(&fcount, 4, 1, mp);
	fcount = le32(fcount);
	pkg = malloc(sizeof(Pkg));
	pkg->count = fcount;
	pkg->entries = calloc(pkg->count + 1, sizeof(Pkg_entry));
	uint32_t i;
	for(i = 0; i < pkg->count; i++) {
		mread(&pkg->entries[i].offset, 4, 1, mp);
		pkg->entries[i].offset = le32(pkg->entries[i].offset);
		if(i != 0)
			pkg->entries[i - 1].size = pkg->entries[i].offset - pkg->entries[i - 1].offset;
	}
	pkg->entries[i - 1].size = size - pkg->entries[i - 1].offset;
	for(i = 0; i < pkg->count; i++) {
		mseek(mp, pkg->entries[i].offset, SEEK_SET);
		pkg->entries[i].data = malloc(pkg->entries[i].size);
		mread(pkg->entries[i].data, pkg->entries[i].size, 1, mp);
	}
	mclose(mp);
	return pkg;
}

void* compile_pkg(Pkg* pkg, uint32_t* size)
{
	return NULL;
}

void delete_pkg(Pkg* pkg)
{
	int i;
	for(i = 0; i < pkg->count; i++) {
		free(pkg->entries[i].data);
	}
	free(pkg->entries);
	free(pkg);
}

