/*
	sc3hax -- Various tools for Soul Calibur 3
	Memory "file" handler

Copyright (C) 2009~2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "memfile.h"

void mread(void* dst, size_t size, size_t count, Mem* src)
{
	if(!(src->mode & 0x1)) {
		printf("Invalid mode for reading!\n");
		return;
	}
	uint8_t* data = (uint8_t*)dst;
	if(((size * count) + src->position) > src->memorysize) {
		printf("Tried to over read %d %d %d %d.\n", size, count, src->position, src->memorysize);
		return;
	}
	memcpy(data, ((uint8_t*)src->memory) + src->position, size * count);
	src->position += size * count;
}

char mgetc(Mem* src)
{
	char ret;
	mread(&ret, 1, 1, src);
	return ret;
}

void mwrite(void* dst, size_t size, size_t count, Mem* src)
{
	if((src->mode & 0x2) == 0) {
		printf("Invalid mode for writing!\n");
		return;
	}
	uint8_t* data = (uint8_t*)dst;
	if(((size * count) + src->position) > src->memorysize) {
		printf("Tried to over write.\n");
		return;
	}
	memcpy(((uint8_t*)src->memory) + src->position, data, size * count);
	src->position += size * count;
}

void mputc(char inchar, Mem* src)
{
	mwrite(&inchar, 1, 1, src);
}

Mem* mopen(void* indata, size_t size, char mode)
{
	Mem* mem = malloc(sizeof(Mem));
	if((mode & 0x2) == 0x0)
		mem->memory = indata;
	else
		mem->memory = calloc(size, 1);
	if(mem->memory == NULL)
		return NULL;
	mem->memorysize = size;
	mem->position = 0;
	mem->mode = mode;
	return mem;
}

void* mclose(Mem* mem)
{
	void* ret = mem->memory;
	mem->memory = NULL;
	mem->memorysize = 0;
	mem->position = 0;
	mem->mode = 0;
	free(mem);
	return ret;
}

void* mgetm(Mem* mem)
{
	return mem->memory;
}

void mseek(Mem* mem, size_t location, int type)
{
	switch(type) {
		case SEEK_CUR:
			mem->position += location;
			break;

		case SEEK_SET:
			mem->position = location;
			break;

		case SEEK_END:
			mem->position = mem->memorysize - location;
			break;
	}
}

size_t mtell(Mem* mem)
{
    return mem->position;
}

size_t msize(Mem* mem)
{
    return mem->memorysize;
}

