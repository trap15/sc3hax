/*
	sc3hax -- Various tools for Soul Calibur 3
	Memory "file" handler

Copyright (C) 2009~2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef MEMFILE_H_
#define MEMFILE_H_

#include <stdlib.h>
#include <stdint.h>

typedef struct {
	void* memory;
	size_t memorysize;
	size_t position;
	char mode;	// 1 = read, 2 = write, 3 = both
} Mem;

void mread(void* dst, size_t size, size_t count, Mem* src);
char mgetc(Mem* src);
void mwrite(void* dst, size_t size, size_t count, Mem* src);
void mputc(char inchar, Mem* src);
Mem* mopen(void* indata, size_t size, char mode);
void* mclose(Mem* mem);
void* mgetm(Mem* mem);
void mseek(Mem* mem, size_t location, int type);
size_t mtell(Mem* mem);
size_t msize(Mem* mem);

#endif // MEMFILE_H_

