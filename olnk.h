/*
	sc3hax -- Various tools for Soul Calibur 3
	OLNK reader

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the MIT license;
# see file LICENSE or http://www.opensource.org/licenses/mit-license.php
*/

#ifndef OLNK_H_
#define OLNK_H_

#include "tools.h"

typedef struct {
	uint32_t offset;
	uint32_t size;
	uint32_t unk1;
	uint32_t unk2;
	void* data;
} Olnk_entry;

typedef struct {
	uint32_t data_off; /* Offset for all files */
	uint32_t count; /* File count */
	Olnk_entry me; /* Olnk entry for this archive */
	Olnk_entry* entries;
} Olnk;

Olnk* load_olnk(void* data, uint32_t size);
void* compile_olnk(Olnk* olnk, uint32_t* size);
void delete_olnk(Olnk* olnk);

#endif

